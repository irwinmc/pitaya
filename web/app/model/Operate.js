Ext.define('Pitaya.model.Operate', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'playerCreateCount',			type: 'int'},
		{name: 'totalRegisterCount',		type: 'int'},
		{name: 'registerConversion',		type: 'int'},
		{name: 'enterGamePlayerCount',		type: 'int'},
		{name: 'loginAccountCount',			type: 'int'},
		{name: 'onlineAvgCount',		    type: 'int'},
		{name: 'onlineMaxCount',			type: 'int'},
		{name: 'totalChargeMoney',			type: 'int'},
		{name: 'totalChargeGold',			type: 'int'},
		{name: 'chargeAccountCount',		type: 'int'},
		{name: 'chargeCount',		    	type: 'int'},
		{name: 'arpu',				        type: 'int'},
		{name: 'activePayPermeability',	    type: 'int'}
	],
	proxy: {
		type: 'ajax'
	}
});