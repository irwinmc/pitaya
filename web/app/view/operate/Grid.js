Ext.define('Pitaya.view.operate.Grid', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.operategrid',

	store: 'Operates',

	title: '运营基本数据',
	border: false,

	viewConfig: {
		emptyText: "查询记录为空"
	},

	initComponent: function() {
		this.columns = [
			{header: '创建角色数',		dataIndex: 'playerCreateCount',		flex: 1},
			{header: '累计注册人数',		dataIndex: 'totalRegisterCount',	flex: 1},
			{header: '进入游戏用户数',	dataIndex: 'enterGamePlayerCount',	flex: 1},
			{header: '登录人数',			dataIndex: 'loginAccountCount',		flex: 1},
			{header: '平均在线人数',		dataIndex: 'onlineAvgCount',		flex: 1},
			{header: '最高在线人数',		dataIndex: 'onlineMaxCount',		flex: 1},
			{header: '累计充值',			dataIndex: 'totalChargeMoney',		flex: 1},
			{header: '累计充值（金币）',	dataIndex: 'totalChargeGold',		flex: 1},
			{header: '充值人数',			dataIndex: 'chargeAccountCount',	flex: 1},
			{header: '充值次数',			dataIndex: 'chargeCount',			flex: 1},
			{header: 'ARPU',			dataIndex: 'arpu',					flex: 1}
		];

		this.tbar = [{
			xtype: 'combo',
			itemId: 'checkprofile',
			fieldLabel: '档案',
			labelWidth: 65,
			width: 300,
			displayField: 'name',
			valueField: 'id',
			editable: false,
			emptyText: '请选择档案',
			store: 'Profiles'
		}, {
			xtype: 'datefield',
			itemId: 'startdate',
			fieldLabel: '开始日期',
			labelWidth: 40,
			editable: false,
			format: 'Y-m-d',
			emptyText: '请选择日期',
			value: new Date()
		}, {
			xtype: 'datefield',
			itemId: 'enddate',
			fieldLabel: '结束日期',
			labelWidth: 65,
			editable: false,
			format: 'Y-m-d',
			emptyText: '请选择日期',
			value: new Date()
		}, {
			text: '查询',
			handler: this.onSearch
		}];

		this.bbar = Ext.create('Ext.PagingToolbar', {
			store: 'Operates',
			displayInfo: true,
			displayMsg: '记录 {0} - {1} of {2}',
			emptyMsg: '没有记录'
		});

		this.callParent(arguments);
	},

	onSearch: function() {
		var grid = Ext.getCmp('operategrid'),
			tableid = grid.down("#checkprofile").getValue(),
			startDate = grid.down("#startdate").rawValue,
			endDate = grid.down("#enddate").rawValue;

		grid.store.getProxy().url = SERVER_URL + "servlet/data?ids=1&tableid=" + tableid + "&start-date=" + startDate + "&end-date=" + endDate;
		grid.store.load();
	}
});