Ext.define('Pitaya.view.Menu', {
	extend: 'Ext.tree.Panel',
	alias: 'widget.menu',

	width: 250,
	title: '菜单',

	rootVisible: false,

	initComponent: function() {
		Ext.apply(this, {
			store: new Ext.data.TreeStore({
				root: {
					expanded: true,
					children: [{
						itemId: 'operategrid',
						text: "运营数据",
						leaf: true
					}, {
						itemId: 'operategrid',
						text: "等级分布数据",
						leaf: true
					}, {
						itemId: 'operategrid',
						text: "金币消费数据",
						leaf: true
					}]
				}
			}),
			listeners: {
				'itemclick': {
					fn: function(view, record, item, index, e) {
						var card = Ext.getCmp('viewcard'),
							xtype = record.raw.itemId,
							panel = Ext.getCmp(xtype),
							newCardIndex = card.items.indexOf(panel),
							layout = card.getLayout(),
							activePanel = layout.activeItem,
							activePanelIdx = card.items.indexOf(activePanel);

						if (activePanelIdx !== newCardIndex) {
							layout.setActiveItem(newCardIndex);
						}
					}
				}
			}
		});

		this.callParent();
	}
});