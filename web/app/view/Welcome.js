Ext.define('Pitaya.view.Welcome', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.welcome',

	border: false,
	layout: 'fit',
	bodyPadding: 5,
	html: '欢迎'
});