Ext.define('Pitaya.view.Viewport', {
	extend: 'Ext.container.Viewport',

	requires: [
		'Pitaya.view.Menu',
		'Pitaya.view.Welcome',
		'Pitaya.view.operate.Grid'
	],

	layout: 'border',
	defaults: {
		split: true,
		border: false
	},

	initComponent: function() {
		this.items = [{
			region: 'north',
			height: 70,
			bodyPadding: 5,
			bodyStyle: 'background-color: #3892D3',
			html: ''
		}, {
			region: 'west',
			xtype: 'menu',
			collapsible: true,
			margins: '0 0 5 5',
			collapsed: true
		}, {
			id: 'viewcard',
			region: 'center',
			layout: 'card',
			margins: '0 5 5 5',
			activeItem: 1,
			items: [{
				id: 'welcome',
				xtype: 'welcome'
			}, {
				id: 'operategrid',
				xtype: 'operategrid'
			}]
		}];

		this.callParent(arguments);
	}
});