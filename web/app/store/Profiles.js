Ext.define('Pitaya.store.Profiles', {
	extend: 'Ext.data.Store',

	fields: ['id', 'name'],
	proxy: {
		type: 'ajax',
		url: 'servlet/data?ids=2'
	},
	autoLoad: false
});
