Ext.define('Pitaya.store.Operates', {
	extend: 'Ext.data.Store',
	requires: 'Pitaya.model.Operate',
	model: 'Pitaya.model.Operate'
});