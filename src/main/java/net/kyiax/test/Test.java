package net.kyiax.test;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.GaData;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Kyia
 * Date: 13-10-9
 * Time: 上午10:07
 * To change this template use File | Settings | File Templates.
 */
public class Test {

	private static final String APPLICATION_NAME = "Game-GAnalytics/1.0";

	private static final String TABLE_ID = "ga:77915623";

	private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"), ".store/analytics_sample");

	private static FileDataStoreFactory DATA_STORE_FACTORY;

	private static HttpTransport HTTP_TRANSPORT;

	private static final JsonFactory JSON_FACTORY = new JacksonFactory();

	public static void main(String[] args) {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
			Analytics analytics = initializeAnalytics();
			GaData gaData = executeDataQuery(analytics, TABLE_ID);

			printDataTable(gaData);

		} catch (GoogleJsonResponseException e) {
			System.err.println("There was a service error: " + e.getDetails().getCode() + " : " + e.getDetails().getMessage());
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	private static Credential authorize() throws Exception {
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(Test.class.getResourceAsStream("/client_secrets.json")));
		if (clientSecrets.getDetails().getClientId().startsWith("Enter") || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
			System.out.println("Enter Client ID and Secret from https://code.google.com/apis/console/?api=analytics into analytics-cmdline-sample/src/main/resources/client_secrets.json");
			System.exit(1);
		}

		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, Collections.singleton(AnalyticsScopes.ANALYTICS_READONLY))
				.setDataStoreFactory(DATA_STORE_FACTORY)
				.build();

		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	private static Analytics initializeAnalytics() throws Exception {
		Credential credential = authorize();

		return new Analytics.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	private static GaData executeDataQuery(Analytics analytics, String tableId) throws IOException {
		return analytics.data().ga().get(tableId,           // Table Id.
				"2013-10-08",                               // Start date.
				"2013-11-08",                               // End date.
				"ga:eventValue")                            // Metrics.
				.setDimensions("ga:eventCategory,ga:eventAction,ga:eventLabel,ga:hostName")
//				.setSort("-ga:visits,ga:source")
				.setFilters("ga:hostName==game.arthurcn.com;ga:eventAction==Consume Count")
//				.setMaxResults(25)
				.execute();
	}

	private static void printDataTable(GaData gaData) {
		if (gaData.getTotalResults() > 0) {
			System.out.println("Data Table:");

			for (GaData.ColumnHeaders header : gaData.getColumnHeaders()) {
				System.out.format("%-32s", header.getName());
			}
			System.out.println();

			for (List<String> rowValues : gaData.getRows()) {
				for (String value : rowValues) {
					System.out.format("%-32s", value);
				}
				System.out.println();
			}
		} else {
			System.out.println("No data");
		}
	}
}
