package net.kyiax.servlet;

import net.kyiax.ga.GaFetcher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Kyia
 * Date: 13-10-9
 * Time: 下午1:05
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(urlPatterns = {"/servlet/data"})
public class DataServlet extends HttpServlet {

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");

		String str = "";

		int ids = Integer.parseInt(request.getParameter("ids"));
		switch (ids) {
			// ids=1&tableid=920237&startdate=2013-10-11&enddate=2013-10-11&_dc=1381483476794&page=1&start=0&limit=25
			case 1: {
				String tableId = request.getParameter("tableid");
				String startDate = request.getParameter("start-date");
				String endDate = request.getParameter("end-date");
				int maxResults = Integer.parseInt(request.getParameter("limit"));
				str = GaFetcher.INSTANCE.executeOperateQuery(tableId, startDate, endDate, maxResults);
			} break;
			// ids=2
			case 2: {
				str = GaFetcher.INSTANCE.getLocalProfileList();
			} break;
			default:
				break;
		}

		response.getWriter().write(str);
	}
}
