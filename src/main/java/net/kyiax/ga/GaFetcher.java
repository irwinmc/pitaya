package net.kyiax.ga;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.GoogleUtils;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.Accounts;
import com.google.api.services.analytics.model.GaData;
import com.google.api.services.analytics.model.Profiles;
import com.google.api.services.analytics.model.Webproperties;
import com.google.gson.Gson;
import net.kyiax.Events;
import net.kyiax.analytic.BriefOperate;
import net.kyiax.vo.Profile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Kyia
 * Date: 13-10-9
 * Time: 下午1:20
 * To change this template use File | Settings | File Templates.
 */
public enum GaFetcher {

	INSTANCE;

	private static final Log LOG = LogFactory.getLog(GaFetcher.class);

	// Application name
	private String applicationName = "Game-GAnalytics/1.0";

	// Set the proxy is enabled
	private Boolean enableProxy = true;

	// Directory to store user credentials.
	private java.io.File dataStoreDir = new java.io.File(System.getProperty("user.home"), ".store/analytics_sample");

	// Global instance of the {@link com.google.api.client.util.store.DataStoreFactory}. The best practice is to make it a single
	// globally shared instance across your application.
	private FileDataStoreFactory dataStoreFactory;

	// Global instance of the HTTP transport.
	private HttpTransport httpTransport;

	// Global instance of the JSON factory.
	private JsonFactory jsonFactory = new JacksonFactory();

	// Analytic server object
	private Analytics analytics;

	// My profile ids
	private List<Profile> profileList;

	/**
	 * Initialization
	 */
	public void init() {
		try {
			// Make transport tunnel
			if (enableProxy) {
				httpTransport = newProxyTransport();
			} else {
				httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			}

			// File data store, store the credential of this application
			dataStoreFactory = new FileDataStoreFactory(dataStoreDir);

			// Make analytic service object
			analytics = initializeAnalytics();

			// Get my first profile id
			profileList = getProfileList();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	/**
	 * Create proxy transport
	 *
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */
	private HttpTransport newProxyTransport() throws GeneralSecurityException, IOException {
		NetHttpTransport.Builder builder = new NetHttpTransport.Builder();
		builder.trustCertificates(GoogleUtils.getCertificateTrustStore());
		builder.setProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("127.0.0.1", 8087)));
		return builder.build();
	}

	/**
	 * Authorizes the installed application to access user's protected data.
	 *
	 * @return
	 * @throws Exception
	 */
	private Credential authorize() throws Exception {
		// Load client secrets
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory, new InputStreamReader(GaFetcher.class.getResourceAsStream("/client_secret_340930316772-65q704kgf2s5rjdur42pkie5268a0fk9.apps.googleusercontent.com.json")));
		if (clientSecrets.getDetails().getClientId().startsWith("Enter") || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
			System.out.println("Enter Client ID and Secret from https://code.google.com/apis/console/?api=analytics into analytics-cmdline-sample/src/main/resources/client_secrets.json");
			System.exit(1);
		}

		// Set up authorization code flow
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, jsonFactory, clientSecrets, Collections.singleton(AnalyticsScopes.ANALYTICS_READONLY))
				.setDataStoreFactory(dataStoreFactory)
				.build();

		// Authorize
		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	/**
	 * Performs all necessary setup steps for running requests against the API.
	 *
	 * @return
	 * @throws Exception
	 */
	private Analytics initializeAnalytics() throws Exception {
		// Authorization.
		Credential credential = authorize();

		// Set up and return Google Analytics API client.
		return new Analytics.Builder(httpTransport, jsonFactory, credential).setApplicationName(applicationName).build();
	}

	/**
	 * Traverses down through the Management API hierarchy to retrieve the first view (profile) ID for the authorized user.
	 *
	 * @return
	 * @throws IOException
	 */
	private List<Profile> getProfileList() throws IOException {
		List<Profile> profileList = new ArrayList<Profile>();

		// Query accounts collection.
		Accounts accounts = analytics.management().accounts().list().execute();
		if (accounts.getItems().isEmpty()) {
			LOG.error("No accounts found");
		} else {
			String firstAccountId = accounts.getItems().get(0).getId();

			// Query webproperties collection.
			Webproperties webproperties = analytics.management().webproperties().list(firstAccountId).execute();
			if (webproperties.getItems().isEmpty()) {
				LOG.error("No Webproperties found");
			} else {
				int length = webproperties.getItems().size();
				for (int i = 0; i < length; i++) {
					String webpropertyId = webproperties.getItems().get(i).getId();
					String webpropertyName = webproperties.getItems().get(i).getName();
					// Query profiles collection.
					Profiles profiles = analytics.management().profiles().list(firstAccountId, webpropertyId).execute();
					if (profiles.getItems().isEmpty()) {
						LOG.error("No views (profiles) found");
					} else {
						String profileId = profiles.getItems().get(0).getId();
						Profile profile = new Profile(profileId, webpropertyName);
						profileList.add(profile);
					}
				}
			}
		}
		return profileList;
	}

	/**
	 * Get local profile list
	 *
	 * @return
	 */
	public String getLocalProfileList() {
		return new Gson().toJson(profileList);
	}

	/**
	 * Get and parse the operate data
	 * <p/>
	 * Example:
	 * GaData gaData = analytics.data().ga().get(
	 * "ga:" + tableId,
	 * "2013-09-24",
	 * "2013-10-10",
	 * "ga:eventValue")
	 * .setDimensions("ga:hostName,ga:eventCategory,ga:eventAction,ga:eventLabel")
	 * .setFilters("ga:hostName==game.arthurcn.com;ga:eventCategory==Operation;ga:eventAction==Log")
	 * .setMaxResults(25)
	 * .setSort("")
	 * .setStartIndex(0)
	 * .execute();
	 *
	 * @param tableId
	 * @param startDate
	 * @param endDate
	 * @param maxResults
	 * @return
	 */
	public String executeOperateQuery(String tableId, String startDate, String endDate, int maxResults) {
		BriefOperate briefOperate = new BriefOperate();

		// Analytic data
		String metrics = "ga:eventValue";
		String dimensions = GaUtils.getOperateDimensions();
		String filters = GaUtils.getOperateFilters();

		// Try...
		try {
			GaData gaData = analytics.data().ga().get("ga:" + tableId, startDate, endDate, metrics)
					.setDimensions(dimensions)
					.setFilters(filters)
					.setMaxResults(maxResults)
					.execute();

			// DEBUG
			// LOG.debug("GA data reply: " + gaData.toString());

			// Parse gaData to object
			if (gaData.getTotalResults() > 0) {
				for (List<String> rowValues : gaData.getRows()) {
					String eventLabel = rowValues.get(rowValues.size() - 2);
					int eventValue = Integer.parseInt(rowValues.get(rowValues.size() - 1));

					if (eventLabel.equals(Events.EL_PLAYER_CREATE_COUNT))
						briefOperate.setPlayerCreateCount(eventValue);
					else if (eventLabel.equals(Events.EL_TOTAL_REGISTER_COUNT))
						briefOperate.setTotalRegisterCount(eventValue);
					else if (eventLabel.equals(Events.EL_REGISTER_CONVERSION))
						briefOperate.setRegisterConversion(eventValue);
					else if (eventLabel.equals(Events.EL_ENTER_GAME_PLAYER_COUNT))
						briefOperate.setEnterGamePlayerCount(eventValue);
					else if (eventLabel.equals(Events.EL_LOGIN_ACCOUNT_COUNT))
						briefOperate.setLoginAccountCount(eventValue);
					else if (eventLabel.equals(Events.EL_ONLINE_AVG_COUNT))
						briefOperate.setOnlineAvgCount(eventValue);
					else if (eventLabel.equals(Events.EL_ONLINE_MAX_COUNT))
						briefOperate.setOnlineMaxCount(eventValue);
					else if (eventLabel.equals(Events.EL_TOTAL_CHARGE_MONEY))
						briefOperate.setTotalChargeMoney(eventValue);
					else if (eventLabel.equals(Events.EL_TOTAL_CHARGE_GOLD))
						briefOperate.setTotalChargeGold(eventValue);
					else if (eventLabel.equals(Events.EL_CHARGE_ACCOUNT_COUNT))
						briefOperate.setChargeAccountCount(eventValue);
					else if (eventLabel.equals(Events.EL_CHARGE_COUNT))
						briefOperate.setChargeCount(eventValue);
					else if (eventLabel.equals(Events.EL_ARPU))
						briefOperate.setArpu(eventValue);
					else if (eventLabel.equals(Events.EL_ACTIVE_PAY_PERMEABILITY))
						briefOperate.setActivePayPermeability(eventValue);

				}
			}
		} catch (GoogleJsonResponseException e) {
			LOG.error("There was a service error: " + e.getDetails().getCode() + " : " + e.getDetails().getMessage());
		} catch (Throwable t) {
			t.printStackTrace();
		}

		return new Gson().toJson(briefOperate);
	}
}
