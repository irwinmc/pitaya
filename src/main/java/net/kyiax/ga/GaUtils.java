package net.kyiax.ga;

/**
 * Created with IntelliJ IDEA.
 * User: Kyia
 * Date: 13-10-11
 * Time: 上午10:26
 * To change this template use File | Settings | File Templates.
 */
public class GaUtils {

	/**
	 * The OR operator is defined using a comma (,).
	 * The AND operator is defined using a semi-colon (;).
	 */

	private static final String EQUALS = "==";

	// ==========================================================
	// Operate part
	// ==========================================================

	public static String getOperateDimensions() {
		StringBuilder sb = new StringBuilder();
//		sb.append("ga:hostName");
//		sb.append(",");
		sb.append("ga:eventCategory");
		sb.append(",");
		sb.append("ga:eventAction");
		sb.append(",");
		sb.append("ga:eventLabel");
		return sb.toString();
	}

	public static String getOperateFilters() {
		StringBuilder sb = new StringBuilder();
//		sb.append("ga:hostName" + EQUALS + "game.arthurcn.com");
//		sb.append(";");
		sb.append("ga:eventCategory" + EQUALS + "Operation");
		sb.append(";");
		sb.append("ga:eventAction" + EQUALS + "Log");
		return sb.toString();
	}

	// ==========================================================
	// Gold consume part
	// ==========================================================
}
