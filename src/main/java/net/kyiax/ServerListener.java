package net.kyiax;

import net.kyiax.ga.GaFetcher;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-10-9
 * Time: 下午2:48
 * To change this template use File | Settings | File Templates.
 */
public class ServerListener implements ServletContextListener {

	private static final Log LOG = LogFactory.getLog(ServerListener.class);

	@Override
	public void contextInitialized(ServletContextEvent event) {
		GaFetcher.INSTANCE.init();

		LOG.info("Ga data fetcher initialization complete.");
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {

	}
}
